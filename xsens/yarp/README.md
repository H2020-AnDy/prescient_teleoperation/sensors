# yarp "driver" for xsens

A very basic yarp driver (the one from IIT does not work with recent versions of MVN).

## Compilation
* create a build directory
* run cmake-gui
* choose a compiler (visual 2015, x64 should work)
* configure
* generate
* open in visual, build

## Usage
run xsens.exe

## tested on

* windows x64 (visual c++ 2015)

## dependencies: 
xsens mvn sdk (xstypes.dll required, copy to build directory otherwise must be in path)

