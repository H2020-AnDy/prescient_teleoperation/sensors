/*
 * Copyright: (C) 2010 RobotCub Consortium
 * Author: Paul Fitzpatrick
 * CopyPolicy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT
 */

#include <stdio.h>
#include <map>
#include <yarp/os/Time.h>
#include <yarp/os/Network.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/ResourceFinder.h>
#include <iostream>
#include <xstypes.h>
#include "datagram.h"
#include "udpserver.h"
#include "streamer.h"
#include "portsmap.h"
using namespace yarp::os;
PortsMap ports;

int main(int argc, char ** argv) {
    printf("Hello...\n");
    Time::delay(1);
    printf("...world\n");
	
   //yarp initialization
    yarp::os::Network yarp;
    if (!yarp.checkNetwork()) {
        std::cerr << "Cannot connect to yarp network";
    }
	ResourceFinder rf;
	rf.setVerbose();
	rf.setDefaultConfigFile("xsens.ini"); 
	rf.configure(argc, argv);
	std::string server_name = "localhost";
	int server_port = 9763;
	if (rf.check("host"))
		server_name=rf.find("host").asString();
	if (rf.check("port"))
		server_port=rf.find("port").asInt();

	ports[SPPoseEuler].open("/xsens/PoseEuler");
	ports[SPPoseQuaternion].open("/xsens/PoseQuaternion");
	ports[SPPosePositions].open("/xsens/PosePositions");
	ports[SPMetaScaling].open("/xsens/MetaScaling");
	ports[SPMetaMoreMeta].open("/xsens/MetaMoreMeta");
	ports[SPJointAngles].open("/xsens/JointAngles");
	ports[SPLinearSegmentKinematics].open("/xsens/LinearSegmentKinematics");
	ports[SPAngularSegmentKinematics].open("/xsens/AngularSegmentKinematics");
	ports[SPTrackerKinematics].open("/xsens/TrackerKinematics");
	ports[SPCenterOfMass].open("/xsens/COM");
	ports[SPTimeCode].open("/xsens/TimeCode");

	// xsens initialization
	
	UdpServer udpServer(server_name,server_port,ports);

	// main loop
	while (true)
		XsTime::msleep(100);

    return 0;
}
