#!/usr/bin/env python

import xml.etree.ElementTree as ET
from timeit import default_timer as timer
from time import sleep
import signal
import sys
from math import pi

from mvnx_parser import MvnxParser
from transformations import quaternion_from_euler, quaternion_multiply

import rospy
import rospkg
import tf
import tf2_ros
import geometry_msgs.msg


transform_to_icub_frame_ = True

# yaw, pitch, roll (ROS convention)
segment_transformations = {
    'RightHand': [pi/2, pi, 0],
    'LeftHand': [pi/2, 0, 0]
}
# TODO add RightFoot, LeftFoot (both icub and xsens have foot frame at ankle...)
#       make new middle_of_sole frame?
# Note: hand frames are actually wrists on both xsens and icub


class XsensTFPublisherNode():
    """
    Takes recorded movements from xsens suit as mvnx files, publishes TF tree of human body.
    """
    def __init__(self, filepath):
        self.tf_br = tf2_ros.TransformBroadcaster()
        self.transformer = tf.Transformer(True, rospy.Duration(1.0))

        self.xsens2icub = segment_transformations

        self.mvnx = MvnxParser(filepath)
        self.frame_list = self.mvnx.get_frame_list(filepath)
        self.segment_list = self.mvnx.get_segment_list(filepath)
        self.publish_frames()

    def publish_frames(self):
        """
        Publish CoM ground projection and body segment TFs on loop.
        """
        while not rospy.is_shutdown():
            rospy.loginfo('Publishing TFs from mvnx file. Recording is %.2f secs long.', float(self.frame_list[-1].attrib['time'])/1000.)
            start = timer()

            for (i, frame) in enumerate(self.frame_list):
                # Skip first few in frame list, since they contain calibration stuff(?)
                if (frame.attrib['type'] != 'normal'):
                    continue

                self.positions = self.mvnx.get_from_frame(frame, tag='position')
                self.orientations = self.mvnx.get_from_frame(frame, tag='orientation')

                t = float(frame.attrib['time'])/1000.
                while((timer()-start) < t):
                    sleep(0.003)  # wait until time that frame was recorded

                self.publish_com(frame)
                self.publish_tfs_from_frame(frame)

    def publish_com(self, frame):
        # Find approximate center of mass ground projection
        com_pos = self.mvnx.get_from_frame(frame, tag='centerOfMass')
        com_proj_pos = com_pos[:]
        com_proj_pos[2] = 0.

        # TODO find "correct" way to do get orientation of center of mass (difficult, since it's not a real thing)
        # Actually, center of mass task will not take orientation into account so it doesn't matter.

        # Get average yaw (assume x and y axes parallel to ground, since this is projection)
        lf_rot = self.convert_quat_xsens2tf(self.get_segment_rot(21))
        rf_rot = self.convert_quat_xsens2tf(self.get_segment_rot(17))
        left_foot_yaw = tf.transformations.euler_from_quaternion(lf_rot)[2]
        right_foot_yaw = tf.transformations.euler_from_quaternion(rf_rot)[2]
        com_yaw = (left_foot_yaw + right_foot_yaw) / 2.
        com_quat = tf.transformations.quaternion_from_euler(0., 0., com_yaw)

        com_proj = self.make_transform_msg(com_proj_pos, com_quat, 'com_footprint')
        com = self.make_transform_msg(com_pos, com_quat, 'com')
        self.tf_br.sendTransform(com_proj)
        self.tf_br.sendTransform(com)

    def publish_tfs_from_frame(self, frame):
        """
        Publish frames for each body segment
        """
        for seg_id in range(23):   # note that real segment id is this +1
            pos = self.get_segment_pos(seg_id)
            rot = self.convert_quat_xsens2tf(self.get_segment_rot(seg_id))
            name = self.segment_list[seg_id].attrib['label']

            if(transform_to_icub_frame_):
                rot = self.transform_rot_icub_frame(name, rot)

            t = self.make_transform_msg(pos, rot, name)
            self.tf_br.sendTransform(t)

    def get_segment_pos(self, seg_id):
        pos_start_idx = seg_id*3
        return self.positions[pos_start_idx:pos_start_idx+3]

    def get_segment_rot(self, seg_id):
        """
        Output quaternions will be in wxyz form (xsens convention)
        """
        rot_start_idx = seg_id*4
        return self.orientations[rot_start_idx:rot_start_idx+4]


    def make_transform_msg(self, pos, rot, frame_name):
        """
        Make TransformStamped message from a list of pos and rot values.
        """
        t = geometry_msgs.msg.TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = 'robot_map'
        t.child_frame_id = 'xsens/' + frame_name

        t.transform.translation.x = pos[0]
        t.transform.translation.y = pos[1]
        t.transform.translation.z = pos[2]
        t.transform.rotation.x = rot[0]
        t.transform.rotation.y = rot[1]
        t.transform.rotation.z = rot[2]
        t.transform.rotation.w = rot[3]

        return t

    def convert_quat_xsens2tf(self, quat):
        """
        Helper function to switch order of quaternion values.
        Xsens uses wxyz, ROS/tf uses xyzw.
        """
        quat_tf = quat
        quat_tf[0], quat_tf[1], quat_tf[2], quat_tf[3] = quat[1], quat[2], quat[3], quat[0]
        return quat_tf

    def transform_rot_icub_frame(self, name, q):
        if name in self.xsens2icub:
            euler = self.xsens2icub[name]
            q_transform = quaternion_from_euler(euler[0], euler[1], euler[2], 'rzyx')
            return quaternion_multiply(q, q_transform)
        else:
            return q


if __name__ == '__main__':

    filename = rospy.get_param("mvnx_filename", "arm_swings.mvnx")

    rospack = rospkg.RosPack()
    filepath = rospack.get_path('xsens_replay') + '/data/' + filename

    rospy.init_node('mvnx_segtf_publisher')

    try:
        seg_pub_node = XsensTFPublisherNode(filepath)
    except rospy.ROSInterruptException:
        pass

# def signal_handler(signal, frame):
#         sys.exit(0)
# signal.signal(signal.SIGINT, signal_handler)
# signal.pause()
