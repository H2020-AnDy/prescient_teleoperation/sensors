#include <yarp/os/all.h>
#include "eglove.h"

using namespace yarp::os;

class YarpGlove: public RFModule
{
protected:
  std::string name;
  std::string server_name;
  int period;
  BufferedPort<Bottle> rawdata_port;
  BufferedPort<Bottle> contact_port;
  BufferedPort<Bottle> contact_event_port;
  double force_thresholds[4]={2.0,2.0,2.0,2.0}; // force above threshold means contact
  int force_last[4]={-1,-1,-1,-1}; // remembers last contact
  EGlove * glove;
public:
  YarpGlove(): RFModule(), name(""),server_name("localhost")
  {
  }
  
  bool configure(ResourceFinder &rf)
  {
    name=rf.check("name",Value("eglove")).asString();
    server_name=rf.check("server_name",Value("localhost")).asString();
    period=rf.check("sampling_period",Value(400)).asInt();
    if (rf.check("contact_thresholds"))
    {
      Bottle *b=rf.find("contact_thresholds").asList();
      if (b && (b->size()==4))
      {
        for (int i=0;i<4;i++)
        {
          force_thresholds[i]=b->get(i).asDouble();
        }
      }
    }
    rawdata_port.open("/eglove/rawdata:o");
    contact_port.open("/eglove/contact:o");
    contact_event_port.open("/eglove/contact_event:o");
    
    yInfo("EGlove server name: %s",server_name.c_str());
    yInfo("EGlove sampling period: %d",period);
    yInfo("EGlove contact thresholds: %f %f %f %f",force_thresholds[0],
            force_thresholds[1],
            force_thresholds[2],
            force_thresholds[3]);
              
    glove=new EGlove(server_name);
    glove->get_sessions();
    glove->discover_sensors();
    glove->create_session(1);
    glove->add_sensor(1);
    glove->change_parameter("Hand2000","SamplingRate",std::to_string(period));
    glove->start_capture(1);
    return true;
  }

  bool close()
  {
    yInfo("Closing - stopping capture");
    glove->stop_capture(1);
    yInfo("        - deleting session");
    glove->delete_session(1);
    yInfo("        - disconnecting");
    delete glove;
    return true;
  }

  double getPeriod()
  {
    // the e-glove sends the date every ~1s or so, no need to be faster here (yet)    
    return 0.5;
  }
  
  void signal_contact(int i,int contact)
  {
    Bottle& b=contact_event_port.prepare();
    b.clear();
    b.addInt(i);
    b.addInt(contact);
    contact_event_port.write();
  }
  
  bool updateModule()
  {
    std::string rawdata=glove->get_session_live_data(1);
    auto samples=glove->rawdata_to_samples(rawdata);            
    // rawdata output 
    Bottle& output=rawdata_port.prepare();
    output.clear();

    for(auto& sample : samples)
    {
      Bottle& l=output.addList();
      for(auto i : sample)
      {
        l.addDouble(i);
      }
    }
    rawdata_port.write();
    
    // contact (from forces) output 
    Bottle &output2=contact_port.prepare();
    output2.clear();

    for(auto& sample : samples)
    {
      Bottle& l=output2.addList();
      for(unsigned int i=0;i<4;++i)
      {
        int contact=0;
        if (sample[i]>force_thresholds[i])
        {
            contact=1;        
        }
        l.addInt(contact);
        if ((1-contact)==force_last[i])
        {
          signal_contact(i,contact);
        }
        force_last[i]=contact;
      }
    }
    contact_port.write();
    return true;
  }
};

int main(int argc,char **argv)
{ 
  Network yarp;
  if (!yarp.checkNetwork())
  {
    yError("Can not find yarp Network: is yarpsever running happily ?");
    return EXIT_FAILURE;
  }
  
  ResourceFinder rf;
  rf.configure(argc,argv);
  YarpGlove yg;
  int r=EXIT_FAILURE;
  
  try
  {
    r=yg.runModule(rf);
  }  
  catch (std::exception& e)
  {
    yError("EGlove: Exception caught: %s",e.what());
  }
  return r;  
}
