#ifndef _EGLOVE_H
#define _EGLOVE_H

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/date_time.hpp>
#include <boost/format.hpp>

class EGlove
{
protected:   
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::resolver resolver;    
    boost::asio::ip::tcp::socket socket;
    std::string host;
    std::string port;
    bool connected;
public:
  EGlove(const std::string &host, const std::string &port="8080", bool connection=true);  
  bool is_connected() const;
  void connect();  
  std::string request_reply(const std::string &req);
  void get_sessions(void);
  void discover_sensors(void);
  void create_session(unsigned int ses);
  void delete_session(unsigned int ses);
  void add_sensor(unsigned int ses,const std::string &sen="Hand2000");
  void remove_sensor(unsigned int ses,const std::string &sen="Hand2000");
  void change_parameter(const std::string &sen="Hand2000",
                        const std::string &name="SamplingRate", 
                        const std::string &val="400");
  void start_capture(unsigned int ses);
  void stop_capture(unsigned int ses);
  std::string get_session_data(unsigned int ses);
  std::string get_session_live_data(unsigned int ses);
  void synchronize_time(void);
  // Send a SYNC command. This makes use of the serial port on the server, the glove must be connected.
  // This is recommended before each session.
  std::vector< std::vector<double> > rawdata_to_samples(std::string rawdata);
  // build a 2D vector of samples from a json string such as 
  // the one returned by get_session_data or get_session_live_data
  void print_samples(std::vector< std::vector<double> > &samples);
};

#endif
