#include <eglove.h>
#include <string>

void usage(void)
{
  std::cout << "Usage: stop_capture <server addr or ip> <session id>" << std::endl;
}

int main(int argc,char **argv)
{
  if (argc!=3)
  {
    usage();
    return EXIT_FAILURE;
  } 
  
  try
  {
    EGlove glove(argv[1]);   
    glove.stop_capture(std::stoi(argv[2]));
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception caught: " << e.what() << std::endl;
  }

  return EXIT_SUCCESS;
}
