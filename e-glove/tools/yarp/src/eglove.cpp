#include "eglove.h"

EGlove::EGlove(const std::string &host, const std::string &port, bool connection)
: resolver(io_service),
  socket(io_service),
  host(host),
  port(port),
  connected(false)
{
  if (connection)
  {
    connect();
  }
}

bool EGlove::is_connected() const
{
  return connected;
}

void EGlove::connect()
{
    boost::asio::ip::tcp::resolver::query query(host,port);
    boost::asio::ip::tcp::resolver::iterator endpoint_iterator=resolver.resolve(query);
    boost::asio::connect(socket,endpoint_iterator);    
    std::cout << "connected" << std::endl;
    connected=true;
}

std::string EGlove::request_reply(const std::string &req)
{
    if (!connected)
    {
      throw std::runtime_error("Cannot talk to the server (not connected)");
    }
    boost::asio::streambuf request;
    std::ostream request_stream(&request);
    request_stream << "GET " << req << " HTTP/1.1\r\n";
    request_stream << "Host: " << host << "\r\n";
    request_stream << "Accept: */*\r\n\r\n";
    
    boost::asio::write(socket,request);
    boost::asio::streambuf response;
    boost::asio::read_until(socket,response,"\r\n");

    std::istream response_stream(&response);
    
    std::string http_version;
    unsigned int status;
    std::string reason;
    
    response_stream >> http_version;
    response_stream >> status;
    std::getline(response_stream,reason);

    if (status!=200)
    {
      std::cerr << "** error: status: " << status << " (instead of 200)" << std::endl;
      std::cerr << "          reason: " << reason << std::endl;
      throw std::runtime_error("Failed to chat with the server");
    }
    
    boost::asio::read_until(socket, response, "\r\n\r\n");

    int content_lenght=-1;
    std::string l;
    while (std::getline(response_stream,l) && (l!= "\r"))
    {      
      static const boost::regex r_content_lenght("Content\\-Length\\: ([0-9]+)\r");
      boost::smatch result;
      if (boost::regex_match(l,result,r_content_lenght)) 
      {        
          content_lenght=std::stoi(result[1]);
      }
    }
    boost::asio::read(socket,response,
                      boost::asio::transfer_exactly(content_lenght-response.size()));

    boost::asio::streambuf::const_buffers_type bufs=response.data();
    return std::string(boost::asio::buffers_begin(bufs),
                      boost::asio::buffers_begin(bufs)+response.size());
}

void EGlove::get_sessions(void)
{
  std::stringstream json;
  std::string s=request_reply("/InteractAppService/rest/getsessions/");
  json << s;
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(json,pt);
  std::cout << s <<std::endl;
}

void EGlove::discover_sensors(void)
{
  std::stringstream json;
  std::string s=request_reply("/InteractAppService/rest/discoversensors/");
  json << s;
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(json,pt);
  std::cout << s <<std::endl;
}

void EGlove::create_session(unsigned int ses)
{
  std::stringstream req;
  req << "/InteractAppService/rest/createsession/?ses=";
  req << ses;
  std::string s=request_reply(req.str());
}

void EGlove::delete_session(unsigned int ses)
{
  std::stringstream req;
  req << "/InteractAppService/rest/deletesession/?ses=";
  req << ses;
  std::string s=request_reply(req.str());
}

void EGlove::add_sensor(unsigned int ses,const std::string &sen)
{
  std::stringstream req;
  req << "/InteractAppService/rest/addsensor/?ses=";
  req << ses;
  req << "&sen=";
  req << sen;
  std::string s=request_reply(req.str());
}

void EGlove::remove_sensor(unsigned int ses,const std::string &sen)
{
  std::stringstream req;
  req << "/InteractAppService/rest/removesensor/?ses=";
  req << ses;
  req << "&sen=";
  req << sen;
  std::string s=request_reply(req.str());
}

void EGlove::change_parameter(const std::string &sen,
                      const std::string &name, 
                      const std::string &val)
{
  std::stringstream req;
  req << "/InteractAppService/rest/changeparameter/?sen=";
  req << sen;
  req << "&name=";
  req << name;
  req << "&val=";
  req << val;
  std::string s=request_reply(req.str());    
}

void EGlove::start_capture(unsigned int ses)
{
  std::stringstream req;
  req << "/InteractAppService/rest/startcapture/?ses=";
  req << ses;
  std::string s=request_reply(req.str());
}

void EGlove::stop_capture(unsigned int ses)
{
  std::stringstream req;
  req << "/InteractAppService/rest/stopcapture/?ses=";
  req << ses;
  std::string s=request_reply(req.str());
}

std::string EGlove::get_session_data(unsigned int ses)
{
  std::stringstream req;
  req << "/InteractAppService/rest/getsessiondata/?ses=";
  req << ses;
  return request_reply(req.str());
}

std::string EGlove::get_session_live_data(unsigned int ses)
{
  std::stringstream req;
  req << "/InteractAppService/rest/getsessionlivedata/?ses=";
  req << ses;
  return request_reply(req.str());
}

void EGlove::synchronize_time(void)
// Send a SYNC command. This makes use of the serial port on the server, the glove must be connected.
// This is recommended before each session.
{
  request_reply("synchronizetime");
}

std::vector< std::vector<double> > EGlove::rawdata_to_samples(std::string rawdata)
// build a 2D vector of samples from a json string such as 
// the one returned by get_session_data or get_session_live_data
{
  std::vector< std::vector<double> > samples;
  std::stringstream json;
  json << rawdata;
  
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(json,pt);
  // we have a single_item list (noname) containing a subtre called "Events" which is a list agin. Let's iterate over that list.
  for (auto& item : pt.get_child(".Events."))
  {  
    // EvtValues
    samples.push_back(std::vector<double>());
    for (auto& valitem : item.second.get_child("EvtValues"))
    {
      samples.back().push_back(valitem.second.get_child("EvtValue").get_value<double>());
    }
    
    // timestamps
    std::string ts=item.second.get_child("EvtTime").get_value<std::string>();
    // we need to remove the last character and the "T" between date and time
    // for time_from_string to work
    ts.pop_back();
    ts[ts.find_first_of('T')]=' ';      
    boost::posix_time::ptime pt = boost::posix_time::time_from_string(ts);
    static const boost::posix_time::ptime epoch=boost::posix_time::time_from_string("1970-01-01 00:00:00.000");
    double t=((pt-epoch).total_milliseconds())/1000.0;

    samples.back().push_back(t);      
  }     
  return samples;
}

void EGlove::print_samples(std::vector< std::vector<double> > &samples)
// helper: print samples to stdout
{    
  std::for_each(samples.begin(),samples.end(), 
                [](std::vector<double> &v)
                {
                  std::for_each(v.begin(),v.end(),
                                [](double x)
                                {
                                  std::cout << boost::format("%.06f")%x << " ";
                                });
                  std::cout << std::endl;
                  
                } );
}
