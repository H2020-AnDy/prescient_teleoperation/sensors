#include <yarp/os/all.h>
#include <eglove.h>
#include "json_sample.h"

using namespace yarp::os;

int main(int argc,char **argv)
{ 
  Network yarp;
  BufferedPort<Bottle> port;
  port.open("/eglove");
  
  try
  {
    EGlove glove("localhost","8080",false);
    auto samples=glove.rawdata_to_samples(rawdata);            
    Bottle& output=port.prepare();
    output.clear();
    for(auto sample=samples.begin();sample!=samples.end();++sample)
    {
      Bottle& l=output.addList();
      for(auto i=sample->begin(); i!=sample->end(); ++i)
      {
        l.addDouble(*i);
      }
    }
    port.write();
    glove.print_samples(samples);
  }
  
  catch (std::exception& e)
  {
    std::cerr << "Exception caught: " << e.what() << std::endl;
  }

  return EXIT_SUCCESS;
}
