#include <eglove.h>
#include "json_sample.h"

void usage(void)
{
  std::cout << "Usage: test_parser" << std::endl;
}

int main(int argc,char **argv)
{
  if (argc!=1)
  {
    usage();
    ::exit(EXIT_FAILURE);
  } 
  
  try
  {
    EGlove glove("localhost","8080",false); // IP & port never actually used
    auto samples=glove.rawdata_to_samples(rawdata);            
    glove.print_samples(samples);
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception caught: " << e.what() << std::endl;
  }

  return EXIT_SUCCESS;
}
