#include <eglove.h>

#ifdef _WIN32
#define sleep _sleep
#endif

void usage(void)
{
  std::cout << "Usage: test_parser <server addr or ip>" << std::endl;
}

int main(int argc,char **argv)
{
  if (argc!=2)
  {
    usage();
    ::exit(EXIT_FAILURE);
  } 
  
  try
  {
    EGlove glove(argv[1]);   
    glove.get_sessions();
    glove.discover_sensors();
    glove.create_session(1);
    glove.add_sensor(1);
    glove.change_parameter();
    glove.start_capture(1);
    sleep(1);
    glove.stop_capture(1);
    std::string rawdata=glove.get_session_data(1);
    
    auto samples=glove.rawdata_to_samples(rawdata);    
    glove.print_samples(samples);
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception caught: " << e.what() << std::endl;
  }

  return EXIT_SUCCESS;
}
