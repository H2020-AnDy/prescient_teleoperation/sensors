#!/usr/bin/env python3
import sys
from eglove import EGlove

def usage():
  print(
"""
usage: ./get_sessions
  print a list of the currently open sessions.
"""
  )

if len(sys.argv)!=1:
  usage()
  sys.exit()

glove=EGlove()
print(glove.get_sessions())
