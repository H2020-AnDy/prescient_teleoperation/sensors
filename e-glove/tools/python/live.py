#!/usr/bin/env python3
import sys
import time
from eglove import EGlove

def usage():
  print(
"""
usage: ./live.py
  stream the data, type CTRL-C to end
"""
  )

if len(sys.argv)!=1:
  usage()
  sys.exit()

glove=EGlove()

glove.create_session(1)
glove.add_sensor()
glove.change_parameter("Hand2000","SamplingRate","100") # needed !
glove.start_capture()
try:
  while True:
    ls=glove.get_session_live_samples()
    glove.print_samples(ls)
    time.sleep(0.1)
finally:
  glove.stop_capture()
  glove.delete_session()
