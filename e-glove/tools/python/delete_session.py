#!/usr/bin/env python3
import sys
from eglove import EGlove

def usage():
  print(
"""
usage: ./delete_session <session id>
  where:
    session id: the session id we want to stop.
"""
  )

if len(sys.argv)!=2:
  usage()
  sys.exit()

sesid=int(sys.argv[1])

glove=EGlove()
glove.delete_session(sesid)


