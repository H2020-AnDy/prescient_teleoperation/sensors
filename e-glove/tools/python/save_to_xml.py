#!/usr/bin/env python3
import sys
import time
# import thread
# from datetime import datetime,timezone
from lxml import etree as ET
from eglove import EGlove

signal_names={
  "TF": "Thumb force (N)",
  "IF": "Index force (N)",
  "MF": "Medium force (N)",
  "PF": "Palm force (N)",
  "TA": "Thumb angle (degrees)",
  "IA": "Index angle (degrees)",
  "MA": "Medium angle (degrees)",
  "time": "timestamp (s)"
}


def usage():
  print(
"""
usage: ./live.py
  stream the data, type CTRL-C to end
"""
  )
  
if len(sys.argv)!=1:
  usage()
  sys.exit()

glove=EGlove("192.168.0.50:8080")  


framerate = 240
samplingRate = round(1/framerate*1000)

glove.create_session(1)
glove.add_sensor()
glove.change_parameter("Hand2000","SamplingRate","4") # needed !

T = time.localtime()
date = str(T.tm_year) + '-' + str(T.tm_mon) + '-' + str(T.tm_mday)
hour = str(T.tm_hour) + '-' + str(T.tm_min) + '-' + str(T.tm_sec)

root = ET.Element('E-glove')
header = ET.SubElement(root, 'header', Date=date, hour=hour, framerate=str(framerate))
signals_all = ET.SubElement(root, 'signals')
frames = ET.SubElement(root, 'frames')

i = 0
for name in signal_names:
  signal = ET.SubElement(signals_all, 'signal', index=str(i), label=name, name=signal_names[str(name)])
  i += 1


glove.start_capture()
try:
  index = 0
  T = time.time()
  print(T)
  print("record data")
  while True:
    try:
      ls=glove.get_session_live_samples()
      for j in range(len(ls["time"])):
        current_time = ls["time"][j]
        if(index == 0):
          start_time = current_time
        frame = ET.SubElement(frames, 'frame', time=str(round((current_time-start_time)*1000, 2)), index=str(index), ts=str(current_time))
        index += 1
        for name in signal_names:
          if(name != "time"):
            data = ET.SubElement(frame, name)
            data.text = str(ls[name][j])
      time.sleep(0.01)
    except KeyboardInterrupt:
      break


finally:
  tree = ET.ElementTree(root)
  name_file = 'eglove-' + date + '_' + hour + '.xml'
  tree.write(name_file, pretty_print=True, xml_declaration=True,   encoding="utf-8")
  print('data saved')
  end_time = time.time()
  glove.stop_capture()
  glove.delete_session()
  
