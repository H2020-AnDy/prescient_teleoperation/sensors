#!/usr/bin/env python3
import http.client
import time
import json
from datetime import datetime,timezone

class EGlove:
  """ This class is the main REST client for the e-glove prototype.
  """
  signal_names={
      "TF": "Thumb force (N)",
      "IF": "Index force (N)",
      "MF": "Medium force (N)",
      "PF": "Palm force (N)",
      "TA": "Thumb angle (degrees)", # TODO check !
      "IA": "Index angle",
      "MA": "Medium angle",
      "time": "timestamp (s)"
  }

  def __init__(self,server="localhost:8080"):
    """
    parameters:
      server: the REST server and port
              (the windows machine connected to the glove, running InteractAppServer)
              default: "localhost:8080"
    """
    self.server=server
    self.conn=http.client.HTTPConnection(server)
    self.current_session=None

  def request_reply(self,req, *args):
    """
    Build a request from the command req (eg. "getsessions") and an optional list of parameters.

    Returns
      the answer (eg. some json, or empty)
    """

    req="/InteractAppService/rest/"+req+"/"

    if len(args):
      req+="?"+"&".join(map(str,args))

    self.conn.request("GET",req)
    r=self.conn.getresponse()
    if r.status!=200:
      raise RuntimeError("%s request failed, status=%d reason: %s"%(req,r.status,r.reason))
    return r.read()

  def get_sessions(self):
    """ returns a list of existing sessions."""
    data=self.request_reply("getsessions")
    return json.loads(data.decode('utf-8'))

  def discover_sensors(self):
    """ returns a list of existing sensors."""
    data=self.request_reply("discoversensors")
    return json.loads(data.decode('utf-8'))

  def create_session(self,sesid):
    """
    Create a session, using the given session id. Most methods need an active session to succeed.
    Return: nothing.
    """
    self.request_reply("createsession","ses=%d"%sesid)
    self.current_session=sesid

  def delete_session(self,sesid=None):
    """
    Delete a session, using the given session id.
    Return: nothing.
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("delete_session failed: no session given and no session created")
      sesid=self.current_session
    self.request_reply("deletesession","ses=%d"%sesid)
    self.current_session=None

  def add_sensor(self,sesid=None,sen="Hand2000"):
    """
    add a sensor to the current session.
    parameters:
      sesid: the session id.
             If None, defaults to the most recent session id (created by create_session)
      sen: the sensor id, defaults to "Hand2000"
    Return: nothing.
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("add_sensor failed: no session given and no session created")
      sesid=self.current_session
    self.request_reply("addsensor","ses=%d"%sesid,"sen=%s"%sen)

  def remove_sensor(self,sesid=None,sen="Hand2000"):
    """
    remove a sensor from the current session.
    parameters:
      sesid: the session id.
             If None, defaults to the most recent session id (created by create_session)
      sen: the sensor id, defaults to "Hand2000"
    Return: nothing.
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("remove_sensor failed: no session given and no session created")
      sesid=self.current_session
    self.request_reply("removesensor","ses=%d"%sesid,"sen=%s"%sen)

  def change_parameter(self,sen="Hand2000",name="SamplingRate",val="400"):
    """
    change a parameter for the given sensor. MUST be called before capture !

    default parameter "SamplingRate" seems to be a sampling period (in ms)
    """
    self.request_reply("changeparameter","sen=%s"%sen,"name=%s"%name,"val=%s"%val)

  def start_capture(self,sesid=None):
    """
    Start to capture data, might take a few seconds.
    parameters:
      sesid: the session id.
             If None, defaults to the most recent session id (created by create_session)
    Return: nothing.
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("start_capture failed: no session given and no session created")
      sesid=self.current_session
    self.request_reply("startcapture","ses=%d"%sesid)

  def stop_capture(self,sesid=None):
    """
    parameters:
      sesid: the session id.
             If None, defaults to the most recent session id (created by create_session)
    Return: nothing.
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("stop_capture failed: no session given and no session created")
      sesid=self.current_session
    self.request_reply("stopcapture","ses=%d"%sesid)

  def get_session_data(self,sesid=None):
    """
    parameters:
      sesid: the session id.
             If None, defaults to the most recent session id (created by create_session)
    Return: the data (a list of dictionaries from the json data)
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("get_session_data failed: no session given and no session created")
      sesid=self.current_session
    data=self.request_reply("getsessiondata","ses=%d"%sesid)
    data=json.loads(data.decode('utf-8'))
    return data[0]

  def get_session_live_data(self,sesid=None):
    """
    Get captured data from sensors included in the live (running) session.
    Every call returns data generated between previous call and this call.

    parameters:
      sesid: the session id.
             If None, defaults to the most recent session id (created by create_session)
    Return: the data (a list of dictionaries from the json data).
            May contain 0 or more samples collected since the last call.
    """
    if sesid is None:
      if self.current_session is None:
        raise RuntimeError("get_session_live_data failed: no session given and no session created")
      sesid=self.current_session
    data=self.request_reply("getsessionlivedata","ses=%d"%sesid)
    data=json.loads(data.decode('utf-8'))
    if len(data):
      return data[0]
    else:
      return None

  def raw_data_to_samples(self,raw_data):
    """ Utility function to convert a string (raw_data) to meaningful samples."""
    data={
    "TF":[],
    "IF":[],
    "MF":[],
    "PF":[],
    "TA":[],
    "IA":[],
    "MA":[],
    "time":[]
    }
    for ev in raw_data["Events"]:
      t=ev["EvtTime"]
      # seems to be in RFC 3339 format. Beware of possible timezone traps ! (UTC here)
      t_unix=datetime.strptime(t,'%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=timezone.utc).timestamp()
      data["time"].append(t_unix)

      for val in ev["EvtValues"]:
        data[val["EvtType"]].append(val["EvtValue"])
    return data

  def get_session_samples(self,sesid=None):
    """
    Return session data as lists of samples (in a dictionary).
    The dictionary keys are taken from the API:
      * TF: Thumb force (N)
      * IF: Index force (N)
      * MF: Medium force (N)
      * PF: Palm force (N)
      * TA: Thumb angle (degrees) # TODO check !
      * IA: Index angle
      * MA: Medium angle
      * time: timestamp (s)
    """
    raw_data=self.get_session_data(sesid)
    return self.raw_data_to_samples(raw_data)

  def get_session_live_samples(self,sesid=None):
    """
    Return session live data as lists of samples (in a dictionary).
    The dictionary keys are taken from the API:
      * TF: Thumb force (N)
      * IF: Index force (N)
      * MF: Medium force (N)
      * PF: Palm force (N)
      * TA: Thumb angle (degrees) # TODO check !
      * IA: Index angle
      * MA: Medium angle
      * time: timestamp (s)
    """
    raw_data=self.get_session_live_data(sesid)
    return self.raw_data_to_samples(raw_data)

  def print_samples(self,samples):
    """
    Helper function to print the samples in a readable way.
    """
    for i in range(len(samples["time"])):
      for k in ("TF","IF","MF","PF","TA","IA","MA","time"):
        print(samples[k][i],end=" ")
      print()

  def synchronize_time(self):
    """
    Send a sync command on the serial port (must be connected).
    Return: nothing.
    """
    self.request_reply("synchronizetime")

if __name__=="__main__":
  glove=EGlove()
  sessions=glove.get_sessions()
  sensors=glove.discover_sensors()
  glove.create_session(1)
  glove.add_sensor()
  glove.change_parameter() # needed !
  glove.start_capture()
  time.sleep(2)
  ld=glove.get_session_live_data()
  time.sleep(1)
  ls=glove.get_session_live_samples()
  glove.stop_capture()
  p=glove.get_session_data()
  data=glove.get_session_samples()
  print("# TF IF MF PF TA IA MA time")
  glove.print_samples(data)
  glove.delete_session()
