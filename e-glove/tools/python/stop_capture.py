#!/usr/bin/env python3
import sys
from eglove import EGlove
# This script can be used to stop capture (eg. after a crash) as most command do not work in capture mode.

def usage():
  print(
"""
usage: ./stop_capture <session id>
  where:
    session id: the session id that is currently in capture mode and needs to stop.
"""
  )

if len(sys.argv)!=2:
  usage()
  sys.exit()

sesid=int(sys.argv[1])

glove=EGlove()
glove.stop_capture(sesid)

