﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chart.aspx.cs" Inherits="InteractClient.Home" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Interact Client - Monitoring</title>
    <link rel="shortcut icon" href="AppIcons/favicon.ico" />
    <link href="Styles/Styles.css?v=" rel="stylesheet" type="text/css"/>

</head>
<body>
    <div id="loading">
        <div>   
            <img src="Images/Loading.gif" width="42" height="42" style="vertical-align:top;"/>  
        </div>
        <div style = "margin-Top:10px; width:250px">
            <span id="loading-msg">Loading</span>
        </div>
    </div>

    <link href="Scripts/ExtJS/theme-crisp/resources/theme-crisp-all.css?v=" rel="stylesheet" type="text/css"/> 

    <script type="text/javascript" src="Scripts/ExtJS/ext-all.js?v="></script>
    <script type="text/javascript" src="Scripts/ExtJS/theme-crisp/theme-crisp.js?v="></script>
    <script type="text/javascript" src="Scripts/Highcharts/highcharts.js?v="></script> 

    <script type="text/javascript" src="Scripts/Localization/Localization.en-GB.js?v="></script>

    <script type="text/javascript" src="Scripts/Emphasis/Emphasis.js?v="></script>
    <script type="text/javascript" src="Scripts/Emphasis/Chart.js?v="></script>

    <script type="text/javascript">
        Ext.Loader.setConfig({
            enabled: true
        });

        Ext.Loader.setPath('Ext.ux', 'Scripts/ExtJS/ux');

        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.panel.*',
        ]);

        Ext.onReady(function () {
            Ext.QuickTips.init();            
            Ext.setGlyphFontFamily('Pictos');
            
            var loading = Ext.get('loading');
            loading.remove();
            Emphasis.Chart.init();
        });

    </script>

    <form id="form1" runat="server"/>
</body>
</html>
