﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="InteractClient.Home" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Interact Client</title>
    <link rel="shortcut icon" href="AppIcons/favicon.ico" />
    <link href="Styles/Styles.css?v=" rel="stylesheet" type="text/css"/>

</head>
<body>
    <div id="loading">
        <div>   
            <img src="Images/Loading.gif" width="42" height="42" style="vertical-align:top;"/>  
        </div>
        <div style = "margin-Top:10px; width:250px">
            <span id="loading-msg">Loading</span>
        </div>
    </div>

    <link href="Scripts/ExtJS/theme-crisp/resources/theme-crisp-all.css?v=" rel="stylesheet" type="text/css"/> 

    <script type="text/javascript" src="Scripts/ExtJS/ext-all.js?v="></script>
    <script type="text/javascript" src="Scripts/ExtJS/theme-crisp/theme-crisp.js?v="></script>

    <script type="text/javascript" src="Scripts/Localization/Localization.en-GB.js?v="></script>

    <script type="text/javascript" src="Scripts/Emphasis/Emphasis.js?v="></script>
    <script type="text/javascript" src="Scripts/Emphasis/modules/SensorGrid.js?v="></script>
    <script type="text/javascript" src="Scripts/Emphasis/modules/SessionExport.js?v="></script>
    <script type="text/javascript" src="Scripts/Emphasis/modules/WifiSettings.js?v="></script>
    <script type="text/javascript" src="Scripts/Emphasis/Home.js?v="></script>

    <script type="text/javascript">
        Ext.Loader.setConfig({
            enabled: true
        });

        Ext.Loader.setPath('Ext.ux', 'Scripts/ExtJS/ux');

        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.panel.*',
        ]);

        Ext.Loader.onReady(function () {
            Ext.override(Ext.DatePicker, { startDay: 1 });

            Ext.override(Ext.form.field.Date, {
                startDay: 1,
                invalidText: LocalizationStrings.DateFormatError,
                onSelect: function (m, d) {
                    var me = this;
                    var currentValue = me.getValue();
                    if (me.isValid() && !Ext.isEmpty(currentValue) && !Ext.isEmpty(d) && Ext.isDate(d))
                        d.setHours(currentValue.getHours(), currentValue.getMinutes(), currentValue.getSeconds(), currentValue.getMilliseconds());
                    me.setValue(d);
                    me.fireEvent('select', me, d);
                    me.collapse();
                }
            });
        });

        Ext.onReady(function () {
            Ext.QuickTips.init();            
            Ext.setGlyphFontFamily('Pictos');
            
            var loading = Ext.get('loading');
            loading.remove();
            Emphasis.Home.init();
        });

    </script>

    <form id="form1" runat="server"/>
    <iframe id="exportframe" name="exportframe" class="x-hidden"></iframe>
    <form id="exportform" method="post" action="" target="exportframe">
    </form>
</body>
</html>
