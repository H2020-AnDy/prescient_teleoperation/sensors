﻿Emphasis.Chart = {         
    
    viewport: null,
    forcesChart: null,
    anglesChart: null,
    monitoring: false,
    lastMonitoredSensor: null,
    chartWindow: 60 * 1000, //1min window
    lastTime: null,
    lastIdent: null,
    filterData: false,
    delayInterval: 100,
    serieColors: {
       "Thumb": '#4572A7',
       "Index": '#AA4643',
       "Middle": '#89A54E',
       "Palm": '#80699B'
    },
    graspExtendedSeries: ['Palm', 'Index'],
    graspExtendedSensorType: 'GraspExtended',
    graspExtendedSamplingRateMillis: 1,
    graspExtendedRefTime: 0,
    isSamplingGraspExtended: false,
    delayedUpdateTask: new Ext.util.DelayedTask(function () {
        Emphasis.Chart.loadCharts();
    }),

    init: function () {
        var me = this;

        Highcharts.setOptions({
            global: {
                useUTC: true
            }
        });

        var comboTemplateWidth = (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) ? '93%' : '100%';    //for inner template (bug for IE older versions)

        Ext.define('Sensor', {
            extend: 'Ext.data.Model',
            fields: [
                { name: 'SenID', type: 'int' },
                { name: 'Node', type: 'string' },
                { name: 'Addr', type: 'string' },
                { name: 'Sensor', type: 'string' },
                { name: 'Type', type: 'string' }
            ],
            idProperty: 'SenID'
        });

        var sensorStore = Ext.create('Ext.data.Store', {
            model: 'Sensor',
            pageSize: 999,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            }
        });

        me.viewport = Ext.create('Ext.Viewport', {
            layout: {
                type: 'fit'
            },
            items: [                
                {
                    layout: { type: 'vbox', align: 'stretch' },
                    flex: 1,
                    border: true,
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            border: true,                    
                            style: {
                                borderStyle: 'none none solid none'
                            },
                            items: [                        
                                {
                                    xtype: 'image',
                                    src: 'AppIcons/LogoSmallEmphasis.png',
                                    width: 213,
                                    height: 59
                                }
                            ]
                        },
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            style: {
                                backgroundColor: '#F5F5F5'
                            },
                            items: [
                                {
                                    xtype: 'combo',
                                    name: 'sensors',
                                    queryMode: 'local',
                                    triggerAction: 'all',
                                    store: sensorStore,
                                    editable: false,
                                    forceSelection: true,
                                    width: 350,
                                    emptyText: LocalizationStrings.SelectHandSensorMsg,
                                    valueField: 'SenID',
                                    displayField: 'Sensor',
                                    displayTpl: new Ext.XTemplate(
                                    '<tpl for=".">' +
                                        '<tpl>{[values["Sensor"]]} [{[values["Node"]]} - {[values["Addr"]]}]</tpl>' +
                                        '<tpl if="xindex < xcount">, </tpl>' +
                                    '</tpl>'
                                    ),
                                    listConfig: {
                                        getInnerTpl: function () {
                                            return '<table width="' + comboTemplateWidth + '"><tr><td align="left">{[values.Sensor]}</td><td align="right" style="padding-right:1px"><span style="color: #003399"><tpl>[{[values.Node]} - {[values.Addr]}]</tpl></span></td></tr></table>';
                                        }
                                    },
                                    listeners: {
                                        change: {
                                            fn: function (self,  newValue, oldValue, eOpts) {
                                                var filterDataCheckbox = self.nextSibling('[name=filterData]');

                                                var record = self.getStore().getById(newValue);
                                                if (record && record.get('Type') == me.graspExtendedSensorType) {
                                                    var filterDataCheckbox = self.nextSibling('[name=filterData]');
                                                    filterDataCheckbox.setReadOnly(true);
                                                    filterDataCheckbox.setValue(true);
                                                }
                                                else
                                                    filterDataCheckbox.setReadOnly(false);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: LocalizationStrings.Monitor,
                                    width: 80,
                                    enableToggle: true,
                                    listeners: {
                                        toggle: {
                                            fn: function (self, pressed, eOpts) {
                                                me.monitor(self, pressed);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'filterData',
                                    boxLabel: LocalizationStrings.FilterData,
                                    listeners: {
                                        change: function (self, newValue) {
                                            me.filterData = newValue;
                                        }
                                    }
                                }
                            ]
                        }
                    ],                   
                    items: [
                        {
                            xtype: 'container',
                            id: 'anglesChartDiv',
                            flex: 1,
                            listeners: {
                                resize: {
                                    fn: function (self, width, height, oldWidth, oldHeight, eOpts) {
                                        try {
                                            if (me.anglesChart)
                                                me.anglesChart.setSize(width, height, false);
                                        } catch (ex) { }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            id: 'forcesChartDiv',
                            flex: 1,
                            listeners: {
                                resize: {
                                    fn: function (self, width, height, oldWidth, oldHeight, eOpts) {
                                        try {
                                            if (me.forcesChart)
                                                me.forcesChart.setSize(width, height, false);
                                        } catch (ex) { }
                                    }
                                }
                            }
                        }                        
                    ]
                }
            ],
            listeners: {
                boxReady: {
                    fn: function (self) {
                        me.loadSensors(self);
                    }
                }
            }
        });
    },
    loadSensors: function(viewport){
        var me = this;

        viewport.setLoading({ msg: LocalizationStrings.Loading });
        Ext.Ajax.request({ 
            url: 'api/monitor/gethandsensors',
            method: 'GET',
            success: function (response) {
                var sensors = Ext.decode(response.responseText);

                var sensorCombo = me.viewport.down('[name=sensors]');
                sensorCombo.getStore().loadRawData({ items: sensors });
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            },
            callback: function () {
                viewport.setLoading(false);
            }
        });
    },
    monitor: function (button, start) {
        var me = this;
        
        var sensorCombo = me.viewport.down('[name=sensors]');
        var sensorID = sensorCombo.getValue();
        var sensor = sensorCombo.getStore().getById(sensorID);
        
        if (!sensor)
            return;

        var sensorCode = sensor.get('Sensor');
        var isGraspExtended = sensor.get('Type') == me.graspExtendedSensorType;

        sensorCombo.clearInvalid();

        if (start && Ext.isEmpty(sensorCode)) {
            button.toggle(!start, true);
            sensorCombo.markInvalid(LocalizationStrings.FieldRequired);
            return;            
        }
        else if(!start && Ext.isEmpty(me.lastMonitoredSensor)){
            button.toggle(start, true);
            return;
        }

        sensorCombo.setDisabled(start);

        Ext.Ajax.request({
            url: Ext.String.format('api/monitor/{0}', start ? 'start' : 'stop'),
            method: 'GET',
            params: {
                sensorCode: start ? sensorCode : me.lastMonitoredSensor
            },
            success: function (response) {
                me.monitoring = start;

                if (!start) {
                    me.lastMonitoredSensor = null;
                    me.delayedUpdateTask.cancel();
                    button.removeCls('livebutton');
                }
                else {
                    me.lastMonitoredSensor = sensorCode;
                    me.lastTime = null;
                    me.lastIdent = null;
                    me.isSamplingGraspExtended = isGraspExtended;
                    me.graspExtendedRefTime = (new Date()).getTime();
                    me.clearCharts();
                    me.delayedUpdateTask.delay(0);
                    button.addCls('livebutton');
                }
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        });
    },
    loadCharts: function () {
        var me = this;
        
        if (!me.monitoring)
            return;
        
        Ext.Ajax.request({
            url: 'api/monitor/get',           
            method: 'GET',
            params: {
                sensorCode: me.lastMonitoredSensor,
                lastTime: me.lastTime,
                lastIdent: me.lastIdent,
                filterData: me.filterData,
                isGraspExtended: me.isSamplingGraspExtended
            },
            success: function (response) {
                if (!me.monitoring)
                    return;

                var data = Ext.decode(response.responseText);
                me.setChartData(data);                
                me.delayedUpdateTask.delay(me.delayInterval);
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        });
    },
    convertIdentToTimestamp: function (ident) {
        var me = this;
        return me.graspExtendedRefTime + me.graspExtendedSamplingRateMillis * ident;
    },
    convertIdentArrayToTimestamp: function (data) {
        var me = this;

        for (var i = 0; i < data.length; i++)
            data[i].x = me.convertIdentToTimestamp(data[i].x);
        return data;
    },
    setChartData: function (data) {
        var me = this;

        if (!me.forcesChart)
            me.createCharts(data);
        else
            me.updateCharts(data);        
    },
    clearCharts: function(){
        var me = this;

        if(me.forcesChart){
            me.forcesChart.destroy();
            me.forcesChart = null;
        }
        if (me.anglesChart) {
            me.anglesChart.destroy();
            me.anglesChart = null;
        }
    },
    getChartOptions: function(isForceChart, data){
        var me = this;

        if (data.length > 0 && data[0].Values.length > 0) {
            var values = data[0].Values;

            if (me.isSamplingGraspExtended) {
                me.lastIdent = values[values.length - 1].x;
                me.lastTime = values[values.length - 1].r;
            }
            else
                me.lastTime = values[values.length - 1].x;
        }

        var now = (new Date()).getTime();
        if (me.isSamplingGraspExtended && me.lastIdent != null)
            now = me.convertIdentToTimestamp(me.lastIdent);
        else if (!me.isSamplingGraspExtended && me.lastTime != null)
            now = me.lastTime;
        var from = now - me.chartWindow;

        var series = [];
        for (var i = 0; i < data.length; i++) {
            var sData = data[i].Values;

            if (me.isSamplingGraspExtended) {
                if (me.graspExtendedSeries.indexOf(data[i].Name) < 0)
                    continue;

                sData = me.convertIdentArrayToTimestamp(sData);
            }

            series.push({
                id: data[i].Name,
                name: data[i].Name,
                data: sData,
                type: 'spline',
                marker: {
                    enabled: false,
                    symbol: 'circle'
                },
                color: me.serieColors[data[i].Name],
                lineWidth: 1,
                events: {
                    show: function (ev) {
                        if (this.chart == me.anglesChart)
                            return;

                        if (me.anglesChart) {
                            var serie = me.anglesChart.get(this.options.id);
                            if (serie)
                                serie.show();
                        }
                    },
                    hide: function (ev) {
                        if (this.chart == me.anglesChart)
                            return;

                        if (me.anglesChart) {
                            var serie = me.anglesChart.get(this.options.id);
                            if (serie)
                                serie.hide();
                        }
                    }
                }
            });
        }

        return {
            chart: {
                renderTo: isForceChart ? 'forcesChartDiv' : 'anglesChartDiv',
                ignoreHiddenSeries: false,
                zoomType: null,
                animation: false,
                plotBackgroundColor: isForceChart ? '#FDFFE0' : '#FFFFFF'
            },
            credits: {
                enabled: false
            },
            title: {
                text: null
            },
            series: series,
            plotOptions: {
                series: {
                    shadow: false,
                    stickyTracking: false
                }
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    enabled: false
                },
                minPadding: 0,
                maxPadding: 0,
                min: from,
                max: now
            },
            yAxis: {
                min: isForceChart ? -5 : -25,
                max: isForceChart ? 40 : 200,
                title: {
                    text: isForceChart ? LocalizationStrings.ForceNewton : LocalizationStrings.AngleDegrees
                },
                labels: {
                    style: {
                        fontSize: '11px'
                    }
                }
            },
            tooltip: {
                followPointer: true,
                formatter: function () {
                    var tooltipContainer = '<div><div>{0}</div></div></div>';
                    var content = Ext.String.format('<span style="color:{0}">{1} {2}</span>: <b>{3}</b>', this.series.color, this.series.name, 
                        isForceChart ? LocalizationStrings.Force : LocalizationStrings.Angle, Ext.util.Format.number(this.y, '0.0'));

                    return Ext.String.format(tooltipContainer, content);
                }
            },
            legend: {
                enabled: isForceChart
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            }
        };
    },    
    createCharts: function(data){
        var me = this;

        var options = me.getChartOptions(true, data.Forces);
        me.forcesChart = new Highcharts.Chart(options);

        var options = me.getChartOptions(false, data.Angles);
        me.anglesChart = new Highcharts.Chart(options);
    },
    updateSerie: function (isForceChart, data) {
        var me = this;
        var chart = isForceChart ? me.forcesChart : me.anglesChart;
        var needsRedraw = false;
        
        var serie = chart.get(data.Name);
        if (!serie)
            return false;

        var newData = data.Values;
        
        if (newData.length > 0) {
            if (me.isSamplingGraspExtended) {
                me.lastIdent = newData[newData.length - 1].x;
                me.lastTime = newData[newData.length - 1].r;
            }
            else
                me.lastTime = newData[newData.length - 1].x;            
        }
        

        if (me.isSamplingGraspExtended)
            newData = me.convertIdentArrayToTimestamp(newData);

        // remove existing chart points before lastTime - window
        if (me.lastTime) {
            var pointsToRemove = 0;

            var now = me.lastTime;
            if (me.isSamplingGraspExtended)
                now = me.convertIdentToTimestamp(me.lastIdent);
            var from = now - me.chartWindow;

            for (var j = 0; j < serie.points.length; j++) {
                if (serie.points[j].x < from)
                    pointsToRemove++;
                else
                    break;
            }
            for (var j = 0; j < pointsToRemove; j++) {
                serie.removePoint(0, false);
                needsRedraw = true;
            }

            chart.xAxis[0].setExtremes(from, now, false);
        }

        // push new points
        for (var j = 0; j < newData.length; j++) {
            serie.addPoint(newData[j], false);
            needsRedraw = true;
        }

        return needsRedraw;
    },
    updateCharts: function (data) {
        var me = this;

        // update forces
        var needsRedraw = false;

        for (var i = 0; i < data.Forces.length; i++) {
            var redraw = me.updateSerie(true, data.Forces[i]);
            if (redraw)
                needsRedraw = true;
        }

        if (needsRedraw)
            me.forcesChart.redraw();


        // update angles
        needsRedraw = false;

        for (var i = 0; i < data.Angles.length; i++) {
            var redraw = me.updateSerie(false, data.Angles[i]);
            if (redraw)
                needsRedraw = true;
        }

        if (needsRedraw)
            me.anglesChart.redraw();
    }
};