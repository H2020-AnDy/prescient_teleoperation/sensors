﻿var Emphasis = {
    consts: {

        APPNAME: 'Interact Client',

        REPORTTIMEOUT: 1200000, //20 mins

        IMPORTTIMEOUT: 900000, //15 mins

        LONGTASKTIMEOUT: 360000, //6 mins

        MAXIMUMTIMEOUT: 80000, //80 sec

        //Cookie
        LOCALECOOKIE: 'RoadAssistance-locale',
        USERCOOKIE: 'RoadAssistance-user',

        //Errors
        CONNECTIONERROR: 0,
        PAGENOTFOUNDERROR: 404,

        //Custom Errors        
        AUTHENTICATIONERROR: 601,
        NOTFOUNDERROR: 602,
        CONFLICTERROR: 603,
        PRECONDITIONFAILEDERROR: 604,
        INTERNALSERVERERROR: 605,
        FILEFORMATERROR: 606,
        FILEPROCESSINGERROR: 607,
        POSTCONDITIONFAILEDERROR: 608,

        LANDMARKNOTFOUND: 613,
        LANDMARKPOSITIONNOTFOUND: 614,

        VEHICLENOTFOUND: 615,
        VEHICLENODATA: 616,

        LICENSEERROR: 617,

        ATTACHMENTSIZEERROR: 618,

        CONCURRENTUSERERROR: 619,
        TIMEOUTAUTHENTICATIONERROR: 620,

        UNDEFINEDID: 0,
        UNDEFINEDVALUE: -1
    },
    
    trim: function (text) {
        return text.replace(/^\s+|\s+$/g, "");
    }
};
