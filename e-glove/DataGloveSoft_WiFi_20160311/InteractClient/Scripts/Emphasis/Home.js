﻿Emphasis.Home = {         
    
    sensorsGrid: null,
    sensorUpdateTask: null,

    init: function () {
        var me = this;

        var viewport = Ext.create('Ext.Viewport', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'toolbar',
                    border: true,                    
                    style: {
                        borderStyle: 'none none solid none'
                    },
                    items: [                        
                        {
                            xtype: 'image',
                            src: 'AppIcons/LogoSmallEmphasis.png',
                            width: 213,
                            height: 59
                        }
                    ]
                },
                {
                    xtype: 'sensorgrid',
                    listeners: {
                        afterrender: {
                            fn: function (self) {
                                self.getStore().load();
                            },
                            single: true
                        }
                    }
                }
            ]
        });

        me.sensorsGrid = viewport.down('sensorgrid');
        me.startSensorsUpdateTask();
    },
    startSensorsUpdateTask: function () {
        var me = this;
        me.sensorUpdateTask = Ext.TaskManager.newTask({
            run: function () {
                if (!Ext.Ajax.isLoading())
                    me.sensorsGrid.getStore().load();
            },
            interval: 1000 * 3
        });
        me.sensorUpdateTask.start();
    }
};