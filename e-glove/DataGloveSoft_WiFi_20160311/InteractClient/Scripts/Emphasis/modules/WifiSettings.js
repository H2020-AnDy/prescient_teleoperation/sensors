﻿Ext.define('Emphasis.modules.WifiSettings', {
    extend: 'Ext.window.Window',
    alias: ['widget.sensorexport'],

    title: LocalizationStrings.WifiTitle,
    width: 350,    
    constrain: true,
    modal: true,
    resizable: false,
    bodyPadding: '10 10 10 10',
    
    checkIP: function (value) {
        var ipPattern = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        return !Ext.isEmpty(value) && ipPattern.test(value);
    },
    initComponent: function() {
        var me = this;

        me.items = [
            {
                xtype: 'form',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'devIP',
                        fieldLabel: LocalizationStrings.WifiDeviceIP
                    },
                    {
                        xtype: 'numberfield',
                        name: 'devPort',
                        fieldLabel: LocalizationStrings.WifiDevicePort,
                        hideTrigger: true,
                        allowDecimal: true
                    },
                    {
                        xtype: 'textfield',
                        name: 'servIP',
                        fieldLabel: LocalizationStrings.WifiServerIP
                    },
                    {
                        xtype: 'numberfield',
                        name: 'servPort',
                        fieldLabel: LocalizationStrings.WifiServerPort,
                        hideTrigger: true,
                        allowDecimal: true
                    },
                    {
                        xtype: 'textfield',
                        name: 'gate',
                        fieldLabel: LocalizationStrings.WifiGateway
                    },
                    {
                        xtype: 'textfield',
                        name: 'mask',
                        fieldLabel: LocalizationStrings.WifiAddressMask
                    },
                    {
                        xtype: 'textfield',
                        name: 'wid',
                        fieldLabel: LocalizationStrings.WifiID
                    },
                    {
                        xtype: 'textfield',
                        name: 'wpass',
                        fieldLabel: LocalizationStrings.WifiPassword
                    },
                    {
                        xtype: 'combo',
                        name: 'wenc',
                        fieldLabel: LocalizationStrings.WifiSecurity,
                        editable: false,
                        emptyText: LocalizationStrings.WifiSelectSecurityType,
                        valueField: 'id',
                        displayField: 'text',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['id', 'text'],
                            data: [
                                { id: 0, text: LocalizationStrings.WifiSecurityNone },
                                { id: 1, text: LocalizationStrings.WifiSecurityWPA },
                                { id: 2, text: LocalizationStrings.WifiSecurityWPA2 },
                                { id: 3, text: LocalizationStrings.WifiSecurityWEP }
                            ]
                        })
                    }
                ]
            }            
        ];

        me.buttons = [
            {
                text: LocalizationStrings.Save,
                handler: function () {
                    me.setSettings();
                }
            }
        ];
        
        me.callParent();
    },
    setSettings: function () {
        var me = this;

        if (Ext.Ajax.isLoading())
            return;
        
        var form = me.down('form');

        var formValues = form.getValues();

        var formValid = true;

        if (!Ext.isEmpty(formValues.devIP) && !me.checkIP(formValues.devIP)){
            me.down('[name=devIP]').markInvalid(LocalizationStrings.InvalidIP);
            formValid = false;
        }
        if (!Ext.isEmpty(formValues.servIP) && !me.checkIP(formValues.servIP)) {
            me.down('[name=servIP]').markInvalid(LocalizationStrings.InvalidIP);
            formValid = false;
        }
        if (!Ext.isEmpty(formValues.gate) && !me.checkIP(formValues.gate)) {
            me.down('[name=gate]').markInvalid(LocalizationStrings.InvalidIP);
            formValid = false;
        }
        if (!Ext.isEmpty(formValues.mask) && !me.checkIP(formValues.mask)) {
            me.down('[name=mask]').markInvalid(LocalizationStrings.InvalidIP);
            formValid = false;
        }        

        if (!formValid || (Ext.isEmpty(formValues.devIP) && Ext.isEmpty(formValues.devPort) &&
            Ext.isEmpty(formValues.servIP) && Ext.isEmpty(formValues.servPort) &&
            Ext.isEmpty(formValues.gate) && Ext.isEmpty(formValues.mask) &&
            Ext.isEmpty(formValues.wid) && Ext.isEmpty(formValues.wpass) && Ext.isEmpty(formValues.wenc)))
            return;

        me.setLoading(LocalizationStrings.Loading);
        Ext.Ajax.request({
            url: 'api/sensor/wifi',
            method: 'GET',
            params: formValues,
            success: function (response) {
                me.fireEvent('savesettings', me);
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            },
            callback: function () {
                me.setLoading(false);
            }
        });
    }
});