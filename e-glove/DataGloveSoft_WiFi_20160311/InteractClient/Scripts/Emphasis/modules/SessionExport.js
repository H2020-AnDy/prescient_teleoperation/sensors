﻿Ext.define('Session', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'SesGuid', type: 'string' },
        { name: 'Start', type: 'date' }
    ],
    idProperty: 'SesGuid'
});

Ext.define('Emphasis.modules.SessionExport', {
    extend: 'Ext.window.Window',
    alias: ['widget.sensorexport'],

    title: LocalizationStrings.ExportSessionData,
    width: 250,    
    constrain: true,
    modal: true,
    resizable: false,
    bodyPadding: '5 5 0 5',
    sessionGuid: '',

    initComponent: function() {
        var me = this;

        var comboTemplateWidth = (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) ? '93%' : '100%';    //for inner template (bug for IE older versions)

        var sessionStore = Ext.create('Ext.data.Store', {
            model: 'Session',
            pageSize: 999,
            proxy: {
                type: 'ajax',
                url: 'api/session/get',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            },
            listeners: {
                beforeload: {
                    fn: function () {
                        me.setLoading(LocalizationStrings.Loading);
                    }
                },
                load: {
                    fn: function (self, records, successful, eOpts) {
                        me.setLoading(false);

                        if (!successful)
                            Ext.Msg.show({
                                title: Emphasis.consts.APPNAME,
                                msg: eOpts.error.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR
                            });
                        else if (records.length > 0)
                            me.down('[name=sessionGuid]').select(records[0]);
                    }
                }
            }
        });
        
        me.layout = {
            type: 'vbox',
            align: 'stretch'
        };

        me.items = [
            {
                xtype: 'combo',
                name: 'sessionGuid',
                store: sessionStore,
                editable: false,
                forceSelection: true,
                flex: 1,
                emptyText: LocalizationStrings.SelectSessionMsg,
                valueField: 'SesGuid',
                displayField: 'SesGuid',
                triggerAction: 'all',
                queryMode: 'local',
                displayTpl: new Ext.XTemplate(
                    '<tpl for=".">' +
                        '<tpl>{[values["SesGuid"]]}</tpl><tpl> [{[ Ext.Date.format(values["Start"], \'' + Date.patterns.LongDateTimeNoYear + '\')  ]}]</tpl>' +
                        '<tpl if="xindex < xcount">, </tpl>' +
                    '</tpl>'
                    ),
                listConfig: {
                    getInnerTpl: function () {
                        return '<table width="' + comboTemplateWidth + '"><tr><td align="left">{[values.SesGuid]}</td><td align="right" style="padding-right:1px"><span style="color: #003399"><tpl>[{[Ext.Date.format(values.Start, \'' + Date.patterns.LongDateTimeNoYear + '\')]}]</tpl></span></td></tr></table>';
                    }
                }
            }
        ];

        me.buttons = [
            {
                text: LocalizationStrings.Export,
                handler: function () {
                    me.doExport();
                }
            }
        ];
        
        me.callParent();

        me.on('show', function () {
            sessionStore.load();
        });
    },
    doExport: function () {
        var me = this;

        var sessionCombo = me.down('[name=sessionGuid]');
        var sessionGuid = sessionCombo.getValue();
        if (!sessionCombo.isValid() || !sessionGuid)
            return;

        Ext.get('exportform').dom.action = Ext.String.format('api/session/export?&_dc={0}&sessionGuid={1}', (new Date()).getTime(), sessionGuid);
        Ext.get('exportform').dom.submit();
    }
});