﻿Ext.define('Sensor', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'SenID', type: 'int' },
        { name: 'NodeID', type: 'int' },
        { name: 'Node', type: 'string' },
        { name: 'DevID', type: 'int' },
        { name: 'Addr', type: 'string' },
        { name: 'Sensor', type: 'string' },
        { name: 'Type', type: 'string' },
        { name: 'SesGuid', type: 'string' },
        { name: 'LastRec', type: 'date', allowNull: true },
        { name: 'LastIdt', type: 'int', allowNull: true },
        { name: 'Values', type: 'string' }
    ],
    idProperty: 'SenID'
});

Ext.define('Emphasis.modules.SensorGrid', {
    extend: 'Ext.grid.Panel',
    alias: ['widget.sensorgrid'],

    flex: 1,
    enableColumnHide: false,
    allowDeselect: true,
    sessionGuid: '',
    capturing: false,
    samplingRate: 400,

    // ref types:
    //selectedIDs: [],
    //toolbarStoreError: null
    //wifiForm: null

    initComponent: function() {
        var me = this;
        
        me.selectedIDs = [];

        me.store = Ext.create('Ext.data.Store', {
            model: 'Sensor',
            pageSize: 999,
            proxy: {
                type: 'ajax',
                url: 'api/sensor/get',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            },
            listeners: {
                beforeload: function () {
                    me.selectedIDs.length = 0;

                    var selections = me.getSelectionModel().getSelection();
                    for (var i = 0; i < selections.length; i++)
                        me.selectedIDs.push(selections[i].get('SenID'));
                },
                load: {
                    fn: function (self, records, successful, eOpts) {
                        if (!successful) {
                            me.toolbarStoreError.setText(eOpts.error.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError);
                            me.toolbarStoreError.setVisible(true);
                        }
                        else
                            me.toolbarStoreError.setVisible(false);

                        var recordsToSelect = [];
                        var sensors = me.getStore().getRange();
                        for (var i = 0; i < sensors.length; i++) {
                            if (me.selectedIDs.indexOf(sensors[i].get('SenID')) >= 0)
                                recordsToSelect.push(sensors[i]);                            
                        }
                        if(recordsToSelect.length > 0)
                            me.getSelectionModel().select(recordsToSelect);
                    }
                },
                metachange: {
                    fn: function (self, meta, eOpts) {
                        if (!Ext.isEmpty(meta)) {
                            var sessionGuid = meta;
                            if (sessionGuid && me.sessionGuid != sessionGuid)
                                me.setSession(sessionGuid);
                        }
                    }
                }
            }
        });

        me.setColumns();
        
        me.dockedItems = [
        {
            xtype: 'toolbar',
            dock: 'top',
            style: {
                backgroundColor: '#F5F5F5'
            },
            items: [
                {
                    xtype: 'tbtext',
                    text: LocalizationStrings.Sensors,
                    style: {
                        fontSize: '15px',
                        color: '#157FCC'
                    }
                },
                {
                    xtype: 'button',
                    icon: 'AppIcons/Chart.png',
                    tooltip: LocalizationStrings.LiveChart,
                    border: false,
                    handler: function () {
                        window.open('Chart.aspx');
                    }
                },
                '->',
                {
                    xtype: 'tbtext',
                    itemId: 'sessionText',
                    width: 190,
                    text: LocalizationStrings.LastSession + ': -'
                },
                {
                    xtype: 'button',
                    icon: 'AppIcons/Serial.png',
                    tooltip: LocalizationStrings.SerialSettings,
                    border: false,
                    menu: [
                        {
                            text: LocalizationStrings.SynchronizeTime,
                            handler: function () {
                                me.synchTime();
                            }
                        },
                        {
                            text: LocalizationStrings.SetWifi,
                            handler: function () {
                                me.setWifi();
                            }
                        }
                    ]
                },                
                {
                    xtype: 'button',
                    text: Ext.String.format(LocalizationStrings.SamplingRate, me.samplingRate),
                    width: 150,
                    handler: function (self) {
                        me.openSamplingRatePopup(self);
                    }
                },
                {
                    xtype: 'button',
                    text: LocalizationStrings.CreateSession,
                    width: 110,
                    handler: function () {
                        me.createSession();
                    }
                },                
                {
                    xtype: 'button',
                    text: LocalizationStrings.Capture,
                    width: 110,
                    enableToggle: true,
                    listeners: {
                        toggle: {
                            fn: function (self, pressed, eOpts) {                                
                                if (!me.sessionGuid) {                                    
                                    self.toggle(!pressed, true);
                                    Ext.Msg.show({
                                        title: Emphasis.consts.APPNAME,
                                        msg: LocalizationStrings.NoSessionMsg,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.INFO
                                    });
                                    return;
                                }

                                me.capture(self, pressed);
                            }
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: LocalizationStrings.Export,
                    width: 110,
                    handler: function () {
                        if (me.capturing) {
                            Ext.Msg.show({
                                title: Emphasis.consts.APPNAME,
                                msg: LocalizationStrings.CapturingMsg,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.INFO
                            });
                            return;
                        }
                        if (!me.sessionGuid) {
                            Ext.Msg.show({
                                title: Emphasis.consts.APPNAME,
                                msg: LocalizationStrings.NoSessionMsg,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.INFO
                            });
                            return;
                        }

                        Ext.create('Emphasis.modules.SessionExport', { sessionGuid: me.sessionGuid }).show();
                    }
                }
            ]
        },
        {
            xtype: 'pagingtoolbar',
            store: me.store,
            dock: 'bottom',
            displayInfo: true,
            displayMsg: LocalizationStrings.Total + ': {2}',
            items: [
                {
                    xtype: 'tbtext',
                    name: 'storeError',
                    text: LocalizationStrings.InternalErrorStore,
                    cls: 'storeError',
                    hidden: true,
                    listeners: {
                        beforerender: {
                            fn: function (self) {
                                me.toolbarStoreError = self;
                            }
                        }
                    }
                }
            ],
            listeners: {
                beforerender: {
                    fn: function (self, eOpts) {                        
                        var i = 0;
                        while (self.getComponent(i)) {
                            if (self.getComponent(i).itemId != 'refresh' && self.getComponent(i).itemId != 'displayItem' && self.getComponent(i).getXType() != 'tbfill')
                                self.getComponent(i).hide();
                            i++;
                        }                        
                    },
                    single: true
                }
            }
		}];

        me.viewConfig = {
            stripeRows: false,
            loadMask: false,
            getRowClass: function (record, rowIndex, rowParams, store) {
                return record.get('SesGuid') != me.sessionGuid ? 'oldSessionRow' : '';
            }
        };

        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
            checkOnly: true,
            mode: 'SIMPLE'
        });       

        me.callParent();
    },
    setColumns: function (){
        var me = this;
        me.columns = [
        {
            text: LocalizationStrings.Node,
            dataIndex: 'Node',
            sortable: false,
            width: 150,
            tdCls: 'verticalAlign',
            renderer: function (value, metadata, record) {
                var code = Ext.util.Format.htmlEncode(value);
                var deviceID = Ext.util.Format.htmlEncode(record.get('DevID'));

                return Ext.String.format('<table style="width: 100%;"><tr><td>{0}</td><td style="width: 45px;text-align:right;">[{1}]</td></tr></table>', code, deviceID);
            }
        },
        {
            text: LocalizationStrings.Address,
            dataIndex: 'Addr',
            sortable: false,
            width: 140,
            tdCls: 'verticalAlign'
        },
        {
            text: LocalizationStrings.Sensor,
            dataIndex: 'Sensor',
            sortable: false,
            width: 120,
            tdCls: 'verticalAlign'
        },
        {
            text: LocalizationStrings.SensorType,
            dataIndex: 'Type',
            sortable: false,
            width: 140,
            tdCls: 'verticalAlign'
        },
        {
            text: LocalizationStrings.Session,
            dataIndex: 'SesGuid',
            sortable: false,
            width: 70,
            tdCls: 'verticalAlign'
        },
        {
            xtype: 'datecolumn',
            text: LocalizationStrings.LastRecorded,
            dataIndex: 'LastRec',
            sortable: false,
            width: 150,
            tdCls: 'verticalAlign',            
            renderer: function (value, meta, record, rowIndex, colIndex, store, view) {
                if (record.get('LastIdt') && record.get('LastIdt') > 0)
                    return record.get('LastIdt');
                return Ext.util.Format.date(value, Date.patterns.NoYearDateTimeMilli);
            }
        },
        {
            text: LocalizationStrings.Values,
            dataIndex: 'Values',
            sortable: false,
            flex: 1,
            tdCls: 'verticalAlign'
        }];
    },
    setSession: function (sessionGuid) {
        var me = this;

        me.sessionGuid = sessionGuid;
        me.down('#sessionText').setText(LocalizationStrings.LastSession + ': ' + (sessionGuid ? sessionGuid : '-'));
    },
    createSession: function (start) {
        var me = this;

        if (me.capturing) {
            Ext.Msg.show({
                title: Emphasis.consts.APPNAME,
                msg: LocalizationStrings.CapturingMsg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
            return;
        }

        var records = me.getSelection();
        var senCodes = [];

        for (var i = 0; i < records.length; i++)
            senCodes.push(records[i].get('Sensor'));
        
        if (senCodes.length == 0) {
            Ext.Msg.show({
                title: Emphasis.consts.APPNAME,
                msg: LocalizationStrings.SelectSensorsMsg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
            
            return;
        }

        Ext.Ajax.request({
            url: 'api/session/create/?dc=' + (new Date()).getTime(),
            method: 'POST',
            params: {
                sensorCodes: senCodes.join(','),
                samplingRate: me.samplingRate
            },
            success: function (response) {
                me.setSession(response.responseText ? response.responseText.trim() : null);
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        });
    },
    capture: function (button, start) {
        var me = this;

        if (!me.sessionGuid) {
            Ext.Msg.show({
                title: Emphasis.consts.APPNAME,
                msg: LocalizationStrings.NoSessionMsg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
            return;
        }

        Ext.Ajax.request({
            url: Ext.String.format('api/session/{0}', start ? 'start' : 'stop'),
            method: 'GET',
            params: {
                sessionGuid: me.sessionGuid
            },
            success: function (response) {
                me.capturing = start;

                if (!start)
                    button.removeCls('livebutton');
                else
                    button.addCls('livebutton');
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        });
    },
    openSamplingRatePopup: function(buttonCaller){
        var me = this;

        Ext.create('Ext.window.Window', {
            title: LocalizationStrings.SetSamplingRate,
            width: 250,
            constrain: true,
            modal: true,
            resizable: false,
            bodyPadding: '5 5 0 5',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'numberfield',
                    name: 'samplingRate',
                    hideTrigger: true,
                    flex: 1,
                    minValue: 1,
                    allowBlank: false,
                    allowDecimals: false,
                    allowExponential: false,
                    value: me.samplingRate
                }
            ],
            buttons: [
                 {
                     text: LocalizationStrings.OK,
                     handler: function (self) {
                         var win = self.up('window');
                         var field = win.down('[name=samplingRate]');
                         var value = field.getValue();

                         if (value != null && field.isValid()) {
                             me.samplingRate = value;
                             buttonCaller.setText(Ext.String.format(LocalizationStrings.SamplingRate, me.samplingRate));
                             win.close();
                         }
                     }
                 }
            ]
        }).show();
    },
    synchTime: function () {
        var me = this;

        if (me.capturing) {
            Ext.Msg.show({
                title: Emphasis.consts.APPNAME,
                msg: LocalizationStrings.CapturingMsg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
            return;
        }

        me.setLoading(LocalizationStrings.Loading);
        Ext.Ajax.request({
            url: 'api/sensor/synch',
            method: 'GET',
            success: function (response) {
            },
            failure: function (response) {
                Ext.Msg.show({
                    title: Emphasis.consts.APPNAME,
                    msg: response.status == 502 ? LocalizationStrings.AppServerConnectionError : LocalizationStrings.InternalError,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            },
            callback: function () {
                me.setLoading(false);
            }
        });
    },
    setWifi: function () {
        var me = this;

        if (me.capturing) {
            Ext.Msg.show({
                title: Emphasis.consts.APPNAME,
                msg: LocalizationStrings.CapturingMsg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
            return;
        }

        if (!me.wifiForm)
            me.wifiForm = Ext.create('Emphasis.modules.WifiSettings', {
                closeAction: 'hide',
                listeners: {
                    savesettings: {
                        fn: function (form) {
                            form.close();
                        }
                    }
                }
            });

        me.wifiForm.show();
    }
});