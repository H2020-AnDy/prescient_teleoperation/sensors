﻿LocalizationStrings = {
    "AngleDegrees": "Angle (Degrees)",
    "Angle": "Angle",
    "Angles": "Angles",
    "Address": "Address",
    "AppServerConnectionError": "Connection to InteractAppServer failed",
    "Capture": "Capture",
    "CapturingMsg": "Capture in progress",
    "CreateSession": "Create Session",
    "DateFormatError": "Date format is not valid",    
    "Export": "Export",
    "ExportSessionData": "Export Session Data",
    "FieldRequired": "Field is required",
    "FilterData": "Filter Data",
    "Force": "Force",
    "Forces": "Forces",
    "ForceNewton": "Force (Newton)",
    "HandSensors": "Hand Sensors",
    "InternalError": "Internal Error",
    "InvalidIP": "Invalid IP",
    "LastRecorded": "Last Update",
    "LastSession": "Current Session",
    "LiveChart": "Live Chart",
    "Loading": "Loading...",
    "Monitor": "Monitor",
    "Node": "Node",
    "NoSessionMsg": "No Session found",
    "OK": "OK",
    "SamplingRate": "Sampling Rate: {0}ms",
    "Save": "Save",
    "SelectSessionMsg": "Select Session...",
    "SelectSensorsMsg": "No Sensors selected",
    "SelectSingleSensorMsg": "Select one sensor of type: GraspExtended",
    "SelectHandSensorMsg": "Select Hand Sensor...",
    "Sensor": "Sensor Group",
    "Sensors": "Sensors",
    "SensorType": "Sensor Group Type",
    "SerialSettings": "Serial Port Settings",
    "Session": "Session",        
    "SetSamplingRate": "Set Sampling Rate",
    "SetWifi": "Set Wifi",    
    "SynchronizeTime": "Sync Time",
    "Total": "Total",
    "Values": "Values",
    "WifiAddressMask": "Address Mask",
    "WifiDeviceIP": "Device IP",
    "WifiDevicePort": "Device Port",    
    "WifiGateway": "Gateway",
    "WifiID": "Wifi ID",
    "WifiPassword": "Wifi Password",
    "WifiSecurity": "Security",
    "WifiSecurityNone": "None",
    "WifiSecurityWEP": "WEP",
    "WifiSecurityWPA": "WPA (TKIP)",
    "WifiSecurityWPA2": "WPA2 (AES)",
    "WifiSelectSecurityType": "Select Security Type...",
    "WifiServerIP": "Server IP",
    "WifiServerPort": "Server Port",
    "WifiServerIP": "Server IP",
    "WifiServerPort": "Server Port",
    "WifiTitle": "Wifi Settings"
};

Date.patterns = {
    LongDate: "j/n/Y",
    AltLongDate: "j/n/Y",
    ShortDate: "j/n/Y",
    AltShortDate: "j/n/Y",
    ShortDateNoYear: "j/n",
    AltShortDateNoYear: "j/n",
    LongTime: "G:i:s",
    ShortTime: "G:i",
    AltShortTime: "H:i",
    LongDateTime: "j/n/Y G:i:s",
    AltLongDateTime: "j/n/Y H:i:s",
    ShortDateTime: "j/n/Y G:i",
    AltShortDateTime: "j/n/Y H:i",
    LongDateTimeNoYear: "j/n G:i:s",
    ShortDateTimeNoYear: "j/n G:i",
    ISODateTime: "c",
    NoYearDateTime: "j/n G:i:s",
    LongTimeTwoDigitYear: "j/n/y G:i:s",
    ShortDateTimeTwoDigitYear: "j/n/y G:i",
    ShortDateTwoDigitYear: "j/n/y",
    ChartShortDateTime: "%d/%m/%Y %H:%M:%S",
    NoYearDateTimeMilli: "j/n G:i:s.u"
};